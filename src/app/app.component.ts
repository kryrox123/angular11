import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombreAdd = ""

  listNombres: string[] = []

  nombreShow = "Seleccione..."
  condicionM = false

  printNombres: string[] = []

  constructor() {
  }

  addName(data:any) {
    this.listNombres.push(this.nombreAdd.trim().toUpperCase())
    data.control.touched = false
    this.nombreAdd = ""
  }

  addPrint() {
    if (this.nombreShow == "Seleccione...") {
      this.condicionM = true
    }
    else {
      this.condicionM = false
      this.printNombres.push(this.nombreShow)
    }
  }

}
